var ScoresView = Backbone.View.extend({
    tagName: 'tr',
    score: 0,
    scoreBoardTemplate: _.template($('#tmpl-scoreboard').html()),
    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        //this.listenTo(this.model, 'destroy', this.remove);
    },
    render: function() {
        this.score = this.model.get('score');

        this.$el.html(this.scoreBoardTemplate(this.model.toJSON()));
        this.$el.css('background-color', '#dff0d8').animate({
            backgroundColor: '#fff'
        }, 1500 );
        return this;
    }
});