var PlayView = Backbone.View.extend({
    el: $('#scoreboardapp'),
    events: {
        'click button': 'addScore'
    },
    scoreTemplate: _.template($('#tmpl-score').html()),
    initialize: function () {
        var ref = new Firebase("https://blazing-torch-631.firebaseio.com"),
            auth = ref.getAuth();

        this.userscore = new Score({
            id: auth.uid.split(":")[1]
        });
        this.userscore.fetch({
            wait: true
        });

        this.current_score = this.$('.score');

        this.listenTo(this.userscore, 'all', this.render);
    },
    addScore: function () {
        this.userscore.score(_.random(1, 20));
    },
    render: function () {
        var score = this.userscore.get('score'),
            earned = score - this.userscore.lastscore;

        this.current_score.html(this.scoreTemplate({
            earned: (earned == score ? 0 : earned),
            current_score: score
        }));
    }
});