(function (window, $) {
    var ref = new Firebase("https://blazing-torch-631.firebaseio.com");

    $('#reg').on('change', function () {
        if ($(this).is(':checked')) {
            $('#confirm-password').closest('.form-group').show();
        } else {
            $('#confirm-password').closest('.form-group').hide();
        }
    });

    $('form').on('submit', function (e) {
        e.preventDefault();

        var $this = $(this),
            email = $.trim($this.find('#email').val()),
            password = $.trim($this.find('#password').val()),
            confirm_password = $.trim($this.find('#confirm-password').val());

        var user = {
            'email': email,
            'password': password
        };

        // reg user and login
        if ($('#reg').is(':checked')) {
            if (confirm_password === password) {
                // register user
                ref.createUser(user, function (error, authData) {
                    if (error) {
                        $('.alert-danger').text('Error while register').show().siblings().hide();
                    } else {
                        $('.alert-success').text('Register Ok!').show().siblings().hide();
                        var scoresRef = ref.child("scores"),
                            user_id = authData.uid.split(":")[1];
                        // add to FireBase
                        scoresRef.child(user_id).set({
                            id: user_id,
                            email: user.email,
                            score: 0
                        }, function (error) {
                            loginUser(user);
                        });
                    }
                });
                // login
            } else {
                $('.alert-danger').text('Check passwords').show().siblings().hide();
            }
        } else { // login
            loginUser(user);
        }

        return false;
    });

    var loginUser = function (user) {
        ref.authWithPassword(user, function (error, authData) {
            if (error) {
                $('.alert-danger').text('Error').show().siblings().hide();
            } else {
                $('.alert-success').text('Ok! Redirecting....').show().siblings().hide();
                setTimeout(function () {
                    document.location = "player.html";
                }, 2000)
            }
        });
    };
}(window, jQuery));