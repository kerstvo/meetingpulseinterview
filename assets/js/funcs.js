$(function () {
    $('#logout').on('click', function (e) {
        e.preventDefault();
        var ref = new Firebase("https://blazing-torch-631.firebaseio.com");
        ref.unauth();
        document.location = 'login.html';
    })
});