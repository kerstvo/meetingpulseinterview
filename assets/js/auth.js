(function (window) {
    var ref = new Firebase("https://blazing-torch-631.firebaseio.com"),
        auth = ref.getAuth();

    if (!auth && document.location.pathname.indexOf('login.html') < 0) {
        document.location = 'login.html';
    } else if (auth && document.location.pathname.indexOf('login.html') > -1) {
        document.location = 'player.html';
    }
}(window));