var ScoreBoard = Backbone.Firebase.Collection.extend({
    model: Score,
    url: "https://blazing-torch-631.firebaseio.com/scores/",
    comparator: function(m1, m2) {
        var score1 = m1.get('score'),
            score2 = m2.get('score');
        return score1 > score2 ? -1 : (score1 == score2 ? 0 : 1);
    }
});