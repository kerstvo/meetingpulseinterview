var Score = Backbone.Firebase.Model.extend({
    defaults: {
        score: 0,
        email: ''
    },
    lastscore: 0,
    url: function() {
        var url = "https://blazing-torch-631.firebaseio.com/scores/";
        if (this.isNew()) {
            return url;
        }
        return url + this.id;
    },
    score: function (points) {
        this.lastscore = this.isNew() ? 0 : this.get('score');
        this.save({
            score: this.lastscore + parseInt(points)
        });
    }
});
